define(['nodeunit', '../Graph', '../Statement', './rdf1'], function(nodeunitNode, Graph, Statement, rdf1) {
	//browsers have the global nodeunit already available
	var nu = nodeunitNode || nodeunit;
	
	return nu.testCase({
  		addingAStatement: function(test) {
			var g = new Graph({});
			var s1 = g.create(rdf1.uris[0], rdf1.predicates[0]);
			test.ok(s1 instanceof Statement);
			test.done();
		},
  		addingStatementObjectBNode: function(test) {
			var g = new Graph({});
			var s1 = g.create(rdf1.uris[0], rdf1.predicates[0]);
			test.ok(s1 instanceof Statement);
			test.done();
        },
	    addingStatementSubjectBNode: function(test) {
			var g = new Graph({});
			var s1 = g.create(undefined, rdf1.predicates[0], {type: 'literal', value: 'hepp'});
			test.ok(s1 instanceof Statement);
			test.done();
  		},
	    bnodeStatementsDifferObjectPosition: function(test) {
			var g = new Graph({});
			var s1 = g.create(rdf1.uris[0], rdf1.predicates[0]);
			test.ok(s1 instanceof Statement);
			var s2 = g.create(rdf1.uris[0], rdf1.predicates[0]);
			test.ok(s1 !== s2);
			test.done();
		},
	    bnodeStatementsDifferSubjectPosition: function(test) {
			var g = new Graph({});
			var s1 = g.create(undefined, rdf1.predicates[0], {type: 'literal', value: 'hepp'});
			test.ok(s1 instanceof Statement);
			var s2 = g.create(undefined, rdf1.predicates[0], {type: 'literal', value: 'hepp'});
			test.ok(s1 !== s2);
			test.done();
		},
	    createNonAssertedStatement: function(test) {
			var g = new Graph({});
			g.create(rdf1.uris[0], rdf1.predicates[0], undefined, false);
			test.ok(g.find().length === 0);
			test.done();
		},
	    duplicatStatementDetected: function(test) {
			var g = new Graph({});
			var s1 = g.create(rdf1.uris[0], rdf1.predicates[0], {type: 'literal', value: 'hepp'});
			var s2 = g.create(rdf1.uris[0], rdf1.predicates[0], {type: 'literal', value: 'hepp'});
			test.ok(s1 === s2);
			test.done();
		},
	    removingStatement: function(test) {
			var g = new Graph({});
			var s1 = g.create(rdf1.uris[0], rdf1.predicates[0], {type: 'literal', value: 'hepp'});
			test.ok(g.find().length === 1);
			g.remove(s1);
			test.ok(g.find().length === 0);	
			test.done();
  		},
		replaceURI: function(test) {
			var g = new Graph();
			g.add(rdf1.uris[0], rdf1.predicates[0], {type: 'literal', value: 'hepp'});
			g.add(rdf1.uris[2], rdf1.predicates[0], rdf1.uris[0]);
			g.replaceURI(rdf1.uris[0], rdf1.uris[1]);
			test.ok(g.find(rdf1.uris[0]).length === 0);
			test.ok(g.find(rdf1.uris[1]).length === 1);
			test.ok(g.find(rdf1.uris[1])[0].getSubject() === rdf1.uris[1]);
			test.ok(g.find(rdf1.uris[2])[0].getValue() === rdf1.uris[1]);
			test.done();
		},
		mergeGraph: function(test) {
			var g1 = new Graph();
			var g2 = new Graph();
			g1.addL("_:1", rdf1.predicates[0], 'one');
			g2.addL("_:1", rdf1.predicates[1], 'two');
			g2.addL("_:1", rdf1.predicates[1], 'three');
			g1.addAll(g2);
			test.ok(g1.find().length === 3, "Merged graph should contain 3 statements");
			test.ok(g1.find("_:1").length === 1, "Conflict of blank nodes in merge");
			var stmts = g1.find(null, rdf1.predicates[1]);
			test.ok(stmts.length === 2, "Predicates have changed during merge");
			test.ok(stmts[0].getSubject() === stmts[1].getSubject(), "Statements subjects with original same blank node are no longer same");
			test.done();
		}
	})
});