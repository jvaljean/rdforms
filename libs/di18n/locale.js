/*global define,__confolio*/
define([
    "require",
    "dojo/_base/kernel",
    "dojo/_base/connect",
    "dojo/_base/array",
    "di18n/i18n"
], function(require, kernel, connect, array, i18n) {

    var allNLSDeps = [], momentPath;
    i18n.setCurrentDefaultLocale(kernel.locale);
    return {
        get: function() {
            return kernel.locale;
        },
        useMoment: function(path) {
            momentPath = (path || "momentl")+"/";
        },
        set: function(locale, onLocaleChange) {
            var oldlocale = kernel.locale;
            kernel.locale = locale;
            i18n.setCurrentDefaultLocale(locale);
            i18n.requireLocalizationDependencies(function() {
                if (typeof momentPath !== "undefined") {
                    require([momentPath+locale], function() {
                        connect.publish("/di18n/localeChange", [locale, oldlocale]);
                        onLocaleChange && onLocaleChange();
                    })
                } else {
                    connect.publish("/di18n/localeChange", [locale, oldlocale]);
                    onLocaleChange && onLocaleChange();
                }
            });
        },
        addNLSDeps: function(deps) {
            allNLSDeps = allNLSDeps.concat(deps);
            deps = array.map(deps, function(dep) {return "di18n/i18n!"+dep});
            require(deps, function() {});
        }
    };
});