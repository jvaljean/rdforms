define({
    root: {
        friendly: "Welcome ${first} ${last}",
        troll: "{{PLURAL:$1|one troll|$1 trolls}}",
        chair: "{{PLURAL:$1|one chair|$1 chairs}}"
    },
    sv: true
})